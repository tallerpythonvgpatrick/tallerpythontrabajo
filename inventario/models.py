from django.db import models


# Create your models here.
class Inventario(models.Model):
    id_inventario = models.AutoField(primary_key=True, unique=True), models.AutoField(primary_key=True, unique=True)
    nombre_inventario = models.CharField(max_length=100, null=False)
    cantidad = models.IntegerField(null=False)
    pertenencia_primaria_producto = models.TextField(max_length=100, null=False)
    pertenencia_secundaria_producto = models.TextField(max_length=100, null=False)

