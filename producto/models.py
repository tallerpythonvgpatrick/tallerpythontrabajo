from django.db import models


# Create your models here.
class Producto(models.Model):
    id_producto = models.IntegerField(null=False), models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=50, blank=False, null=False)
    descripcion_producto = models.CharField(max_length=200, blank=True, null=True)
    categoria_producto = models.CharField(max_length=50, blank=False, null=False)
    sub_categoria_producto = models.CharField(max_length=50, blank=False, null=False)
