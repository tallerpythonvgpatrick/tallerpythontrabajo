from django.db import models


# Create your models here.
class Bodega(models.Model):
    id_bodega = models.IntegerField(null=False), models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, null=False)
    cantidad_producto = models.IntegerField(null=False)
    stock_minimo = models.IntegerField(null=False)
    ubicacion = models.TextField(max_length=100, null=False)
    fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)
