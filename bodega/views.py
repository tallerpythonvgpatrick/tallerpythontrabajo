from django.shortcuts import render
from .models import *
from .forms import *

# Create your views here.
def pruebabodega(request):
    return render(request, "prueba2.html", {})


# Create your views here.
def pruebabodega(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre_producto2 = form_data.get("nombre_producto")
        cantidad_producto2 = form_data.get("cantidad_producto")
        stock_minimo2 = form_data.get("stock_minimo")
        ubicacion2 = form_data.get("ubicacion")
        objeto = Bodega.objects.create(nombre_producto=nombre_producto2, cantidad_producto=cantidad_producto2,stock_minimo=stock_minimo2,ubicacion=ubicacion2)
    contexto = {
        "el_formulario": form,
    }
    return render(request, "prueba2.html", contexto)
