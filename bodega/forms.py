from django import forms

class RegForm(forms.Form):
    nombre_producto = forms.CharField(max_length=100)
    cantidad_producto = forms.IntegerField()
    stock_minimo = forms.IntegerField()
    ubicacion = forms.CharField(max_length=100)


