from django.shortcuts import render
from .models import *
from .forms import *


# Create your views here.
def pruebaadquisiciones(request):
    return render(request, "prueba.html", {})

def pruebaadquisiciones(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre_producto2 = form_data.get("nombre_producto")
        nombre_proveedor2 = form_data.get("nombre_proveedor")
        cantidad_producto2 = form_data.get("cantidad_producto")
        numero_factura2 = form_data.get("numero_factura")
        objeto = Adquisicion.objects.create(nombre_producto=nombre_producto2, nombre_proveedor=nombre_proveedor2, cantidad_producto=cantidad_producto2, numero_factura=numero_factura2)
    contexto = {
        "el_formulario": form,
    }
    return render(request, "prueba.html", contexto)
