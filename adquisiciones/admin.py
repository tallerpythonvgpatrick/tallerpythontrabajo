from django.contrib import admin
from adquisiciones.models import Adquisicion
from producto.models import Producto
from inventario.models import Inventario
from bodega.models import Bodega


class AdminProducto(admin.ModelAdmin):
    list_display = ["nombre_producto", "descripcion_producto", "categoria_producto","sub_categoria_producto"]
    list_filter = ["nombre_producto"]
    class Meta:
        model = Producto

class AdminBodega(admin.ModelAdmin):
    list_display = ["nombre_producto", "cantidad_producto", "stock_minimo", "ubicacion", "fecha_ingreso"]
    list_filter = ["nombre_producto"]
    class Meta:
        model = Bodega

class AdminAdquisicion(admin.ModelAdmin):
    list_display = ["nombre_producto", "nombre_proveedor", "cantidad_producto", "fecha", "numero_factura"]
    list_filter = ["nombre_producto"]
    class Meta:
        model = Adquisicion

class AdminInventario(admin.ModelAdmin):
    list_display = ["nombre_inventario", "cantidad", "pertenencia_primaria_producto", "pertenencia_secundaria_producto"]
    list_filter = ["nombre_inventario"]
    class Meta:
        model = Inventario


# Register your models here.
admin.site.register(Adquisicion,AdminAdquisicion)
admin.site.register(Producto,AdminProducto)
admin.site.register(Inventario,AdminInventario)
admin.site.register(Bodega,AdminBodega)
