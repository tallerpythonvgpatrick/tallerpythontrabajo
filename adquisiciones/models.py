from django.db import models


# Create your models here.
class Adquisicion(models.Model):
    id_adquisicion = models.IntegerField(null=False), models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=50, null=False)
    nombre_proveedor = models.CharField(max_length=50, null=False)
    cantidad_producto = models.CharField(max_length=50, null=False)
    fecha = models.DateTimeField(auto_now_add=True, auto_now=False)
    numero_factura = models.IntegerField(null=False)

