from django import forms

class RegForm(forms.Form):
    nombre_producto = forms.CharField(max_length=100)
    nombre_proveedor  = forms.CharField()
    cantidad_producto = forms.CharField(max_length=50)
    numero_factura = forms.IntegerField()
